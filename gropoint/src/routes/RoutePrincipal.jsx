import { Routes, Route } from "react-router-dom";
import Login from "../screens/login/Login";
import DashboardPrincipals from "../screens/principals/dashboard/DashboardPrincipals";
import Program from "../screens/principals/program/Program";

function RoutePrincipal(){
    return(
        <>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/principal/dashboard" element={<DashboardPrincipals />} />
                <Route path="/principal/program" element={<Program /> } />
            </Routes>
        </>
    )
}

export default RoutePrincipal;