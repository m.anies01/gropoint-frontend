import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import MuiDrawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import AddBoxIcon from '@mui/icons-material/AddBox';
import StarsIcon from '@mui/icons-material/Stars';
import SettingsIcon from '@mui/icons-material/Settings';
import AssignmentIcon from '@mui/icons-material/Assignment';
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import DashboardIcon from '@mui/icons-material/Dashboard';
import logo from "./logo.svg";
import logoSidebar from "./logoSidebar.svg";
import { useNavigate } from "react-router-dom";
import "./Sidebar.css";

const drawerWidth = 240;

const openedMixin = (theme) => ({
        width: drawerWidth,
        transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
        overflowX: 'hidden',
});

const closedMixin = (theme) => ({
        transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
        overflowX: 'hidden',
        width: `calc(${theme.spacing(7)} + 1px)`,
        [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(9)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));


const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        // zIndex: theme.zIndex.drawer + 1,
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
            ...(open && {
                ...openedMixin(theme), '& .MuiDrawer-paper': openedMixin(theme),
            }),
            ...(!open && {
                ...closedMixin(theme), '& .MuiDrawer-paper': closedMixin(theme),
            }),
    }),
);


function Sidebar(props){
    const theme = useTheme();
    let navigate = useNavigate();
    const sideBarLink = [
        {url:'#', name: "Dashboard", icon: <DashboardIcon /> },
        {url:'#', name: "Program", icon: <AddBoxIcon /> },
        {url:'#', name: "Rewards", icon: <StarsIcon /> },
        {url:'#', name: "Master", icon: <SettingsIcon /> },
        {url:'#', name: "Report", icon: <AssignmentIcon /> }
    ];


    return(
        <Drawer variant="permanent" open={props.open}>
            <DrawerHeader>
                <IconButton onClick={props.handleDrawerClose}>
                    {theme.direction === 'rtl' ? <img src={logoSidebar} alt="Logo gropoint"/> : <img src={logo} alt="Logo gropoint"/>}
                </IconButton>
            </DrawerHeader>
            <Divider />
            <List>
                { sideBarLink.map((item, index) => {
                    return(
                        <ListItem button key={ index }>
                            <ListItemIcon> {item.icon} </ListItemIcon>
                            <ListItemText primary={item.name}/>
                        </ListItem>
                    )
                })} 
            </List>
            <Divider />
            <List>
                <ListItem button>
                    <ListItemIcon>
                        <PowerSettingsNewIcon /> 
                    </ListItemIcon>
                    <ListItemText primary="Logout" />
                </ListItem>
            </List>
        </Drawer>
    )
}


export default Sidebar;