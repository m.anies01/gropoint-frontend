import * as React from 'react';
import { styled } from '@mui/material/styles';
import MuiAppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import logoSidebar from "./logoSidebar.svg";
import "./Header.css";

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
    })(({ theme, open }) => ({
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
        }),
        ...(open && {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
        }),
}));

function Header(props){

    return(
        <AppBar position="fixed" open={props.open} className="appbar-style">
            <Toolbar>
                <IconButton color="inherit" aria-label="open drawer" onClick={props.handleDrawerOpen} edge="start"
                    sx={{ marginRight: '36px', ...(props.open && { display: 'none' }), }} >
                {/* <MenuIcon />  */}
                <img src={logoSidebar} alt="Logo gropoint"/>
                </IconButton>
                <Typography variant="h6" noWrap component="div" style={{color:"black"}}> Dashboard </Typography>
                <Typography variant="h6" noWrap component="div" style={{color:"black", marginLeft:800, position:'fixed', opacity:'80%'}}> Welcome, principal </Typography>
            </Toolbar>
        </AppBar>
    )
}

export default Header;