import "./Dashboard.css";
import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Header from "../../../components/header/Header";
import Sidebar from "../../../components/sidebar/Sidebar";

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

function DashboardPrincipals(){
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
};

const handleDrawerClose = () => {
    setOpen(false);
};

    return(
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <Header open={open} handleDrawerOpen={handleDrawerOpen} />
            <Sidebar open={open} handleDrawerClose={handleDrawerClose} theme={theme}/>
            <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                <DrawerHeader />
                <Box id="container-dashboard">
                    {['Total Redeem', 'Membership', 'Program', 'Return Reward'].map((text, index) => (
                        <Box id="box-fitur" key={text}> <p className="fitur-name">{text}</p> </Box>
                    ))}
        
                </Box>
            </Box>
        </Box>
    );
}

export default DashboardPrincipals;