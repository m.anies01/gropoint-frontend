import "./Login.css";
import logo from "./images/logo.svg";
import Grid from "@mui/material/Grid";
import { FormControl, InputLabel,Input, Button } from "@mui/material";
import { useState } from "react";
import { LoginApi } from "../../api/AuthApi";
import { useNavigate } from "react-router-dom";

function Login(){
    let navigate = useNavigate();
    const [form, setForm] = useState({
        username: "",
        password: ""
    });


    function formHandler(events){
        console.log(events.target.value);
        return setForm({
            ...form,
            [events.target.value]: events.target.value,
        });
    }

    function loginHandler(events){
        events.preventDefault();
        navigate("/dashboard");
    }

    return(
        <>
        <Grid container spacing={0} >
            <Grid item xs={7.5} className="img-background">
                <h4 className="copyright">Copyright &copy; GroPoint Website 2021 All Right Reserved</h4>
            </Grid>
            <Grid item xs={4.5} className="form-login">
                <form className="container-login-form">
                    <img src={logo} alt="Logo gropoint"/>
                    <h3 className="text-welcome"> Welcome to GroPoint </h3>
                    <FormControl name="username" className="form-input" variant="standard" fullWidth onChange={formHandler}>
                        <InputLabel htmlFor="component-simple">Username</InputLabel>
                        <Input id="component-simple" />
                    </FormControl>
                    <FormControl name="password" className="form-input" variant="standard" fullWidth onChange={formHandler}>
                        <InputLabel htmlFor="component-simple">Password</InputLabel>
                        <Input id="component-simple" type="password" />
                    </FormControl>
                    <Button className="btn-login" variant="contained" fullWidth onClick={loginHandler}>Login</Button>
                </form>
            </Grid>
        </Grid>
        </>
    )
}

export default Login;