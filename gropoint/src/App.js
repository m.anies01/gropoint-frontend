import './App.css';
import Login from "../src/screens/login/Login";
import Dashboard from "./screens/principals/dashboard/Dashboard";
import Reward from "./screens/principals/rewards/Reward";
import Master from "./screens/principals/masters/Master";
import Program from "./screens/principals/program/Program";
import Report from "./screens/principals/report/Report";
import { Routes, Route } from 'react-router-dom';

function App() {
  return (
    <Routes>
      <Route path='/' element={<Login />}/>
      <Route path='/dashboard' element={<Dashboard />}/>
      <Route path='/program' element={<Program />}/>
      <Route path='/reward' element={<Reward />}/>
      <Route path='/master' element={<Master />}/>
      <Route path='/report' element={<Report />}/>
    </Routes>
  );
}

export default App;
